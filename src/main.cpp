#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#include <SPI.h>
#include <MFRC522.h>
#include <EEPROM.h>
#include <PubSubClient.h>
#include <stdio.h>
#include <HX711_ADC.h>
#include <Servo.h>
#include <ArduinoJson.h>
#include <string.h>
#include <Adafruit_NeoPixel.h>
// #include <HashMap.h>

#define START_CELL 2
#define END_CELL 10
#define EEPROM_CARD_OFFSET 4


#define ADR1 0
#define ADR2 2
#define ADR3 15
#define ADR4 10
#define HX711_DT 5
#define HX711_CLK 4
#define SS_PIN HX711_CLK
#define RST_PIN 5
#define DOOR_SWITCH HX711_DT
#define LED_PIN HX711_CLK // Controller of LEDS
#define LOCK_PWM HX711_CLK

#define LED_COUNT 10*16


enum RFID_STATE {
  RESET = 1,
  ADD = 2,
  USER = 3,
  NO_VALID = 4,
  CANT_READ = 5,
};

enum RFID_CMD {
  NOTHING = 0,
  ADD_MAIN_CARD = 1,
  ADD_RESET_CARD = 2,
};

// enum THREESTATE {
//   TRUE
// }

struct RFID_RESPONSE{
  RFID_STATE state;
  byte uuid[4];
};

Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
HX711_ADC LoadCell[10] =  {{HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK},{HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK}, {HX711_DT, HX711_CLK}};

MFRC522 *rfid; // Instance of the class
MFRC522::MIFARE_Key key;
// Init array that will store new NUID
uint16_t number_of_cards=2;
byte nuidPICC[1023][4];

int selected_cell, second_selected_cell, station_id, update[END_CELL] = {0};
bool scale_flag[END_CELL] = {false}, offset_flag[END_CELL] = {false}, weight_publish_flag[END_CELL] = {false};
float scale[END_CELL] = {1},  test_weight[END_CELL] = {1},  weights[END_CELL] = {1};
long offset[END_CELL] = {0};
bool add_main_card_flag = false, add_reset_card_flag = false;
bool channel_flag = true, send_server_flag = false, login_flag = false, status_door_flag = false, status_ip_flag = false, setup_flag = false;
int temp2=0, led_color=0;
String update_string;

const char* host = "esp8266-webupdate";
const char* ssid = "rightel-4g";  // rightel-4g // hardtech_tenda //"Irancell-TD-i40-A1_9274";   // gRexa_NET
const char* password = "@mkt@2017!ikap";     // @mkt@2017!ikap // qazwsxedc //"379B4AA1AA";     // 09124075426
const char* mqtt_server = "88.99.155.208";

Servo myservo;
int pos = 0;

ESP8266WebServer httpServer(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiClient espClient;
PubSubClient client(espClient);

void switch_cell_channel(int channel){
  switch (channel) {
    case 0:
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,LOW);
      break;
    case 1:
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,LOW);
      break;
    case 2:
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,LOW);
      break;
    case 3:
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,LOW);
      break;
    case 4:
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,LOW);
      break;
    case 5:
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,HIGH);
      break;
    case 6:
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,HIGH);
      break;
    case 7:
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,HIGH);
      break;
    case 8:
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,LOW);
      digitalWrite(ADR4,HIGH);
      break;
    case 9:
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,HIGH);
      break;
    case 10:   //  RC_SS  ;   READ_SWITCH_1
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,LOW);
      break;
    case 11:   //  PWM LOCK
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,LOW);
      break;
    case 12:   //  MOSFET LOCK
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,LOW);
      break;
    case 13:   //  LED
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,LOW);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,HIGH);
      break;
    case 14:   //  BUZZER
      digitalWrite(ADR1,LOW);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,HIGH);
      break;
    case 15:   //  STB
      digitalWrite(ADR1,HIGH);
      digitalWrite(ADR2,HIGH);
      digitalWrite(ADR3,HIGH);
      digitalWrite(ADR4,HIGH);
      break;
  }
  // delay(10);

}

void setup_wifi(){
  delay(10);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void mqtt_reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(),"shelfix","shelfix1")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("log", "reconnected");
      // ... and resubscribe
      client.subscribe("cmd_cell");
      client.subscribe("cmd_rfid");
      client.subscribe("permission/1");
      client.subscribe("cmd_status");
      client.subscribe("setting/1");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void sub_callback(char* topic, byte* payload, unsigned int length){
  // Serial.print("Message arrived [");
  // Serial.print(topic);
  // Serial.print("]: ");
  StaticJsonBuffer<1000> jsonBuffer;
  char *cstring = (char *) payload;
  cstring[length] = '\0';
  String s_payload = String(cstring);
  // Serial.println(s_payload);
  if (strcmp(topic,"cmd_cell") == 0){
    selected_cell = (char)(payload[1]) - '0';
    switch ((char)payload[0]) {
      case 't': //  SET OFFSET
        offset_flag[selected_cell] = true;
        second_selected_cell = (char)(payload[2]) - '0';
        break;

      case 's': //  SET SCALE
        scale_flag[selected_cell] = true ;
        test_weight[selected_cell] = (float)(payload[2] - '0')*1000 + (float)(payload[3] - '0')*100 + (float)(payload[4] - '0')*10 + (float)(payload[5] - '0');
        break;

      case 'r': //  PUBLISH WEIGHT
        weight_publish_flag[selected_cell] = true;
        break;
    }
  }
  if (strcmp(topic,"cmd_rfid") == 0){
    switch ((char)payload[0]) {
      case 'm': //  ADD_MAIN_CARD
        add_main_card_flag = true;
        break;

      case 'r': //  ADD_RESET_CARD
        add_reset_card_flag = true;
        break;
    }
  }
  if (strcmp(topic,"permission/1") == 0){   //TODO: "permission/1" -> "permission-"+station_id
    if (s_payload.indexOf("{\"response\": true}") == 0){
      // Serial.println("HEYYY");
      login_flag = true;
    }
    else
      login_flag = false;
  }
  if (strcmp(topic,"cmd_status") == 0){
    switch ((char)payload[0]) {
      case 'i': //  DOOR_SWITCH
        status_ip_flag = true ;
        break;
      case 'd': //  DOOR_SWITCH
        status_door_flag = true ;
        break;
    }
  }
  if (strcmp(topic,"setting/1") == 0){    //  NOTE: MQTT_MAX_PACKET_SIZE=1024
    JsonObject& root = jsonBuffer.parseObject(s_payload);
    // Serial.println("cache_data:"+root["cache_data"].as<String>());
    // Serial.println("response:"+root["response"].as<String>());
    if(root["response"]){
      update_string = root["cache_data"].as<String>();
      setup_flag = true;
    }
  }

}

RFID_RESPONSE read_rfid() {

  Serial.println("read_rfid");
  switch_cell_channel(10);
  RFID_RESPONSE response = { NO_VALID, {0xFF, 0xFF, 0xFF, 0xFF} };
  Serial.println("read_rfid2");
  if ( ! rfid->PICC_IsNewCardPresent() ){
    response.state = NO_VALID;
    Serial.println("NO_VALID");
    return response;
  }


  if ( ! rfid->PICC_ReadCardSerial()){
    response.state = CANT_READ;
    Serial.println("CANT_READ");
    return response;
  }
  Serial.println("REAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD");
  client.publish("log2","REAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD");
  rfid->PICC_DumpDetailsToSerial(&(rfid->uid)); //dump some details about the card

  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid->PICC_GetType(rfid->uid.sak);
  Serial.println(rfid->PICC_GetTypeName(piccType));

  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
    piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
    piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    response.state = NO_VALID;
    return response;
  }

  // Halt PICC
  rfid->PICC_HaltA();

  // Stop encryption on PCD
  rfid->PCD_StopCrypto1();


  if (rfid->uid.uidByte[0]==EEPROM.read(EEPROM_CARD_OFFSET) && rfid->uid.uidByte[1]==EEPROM.read(EEPROM_CARD_OFFSET+1)
      && rfid->uid.uidByte[2]==EEPROM.read(EEPROM_CARD_OFFSET+2) && rfid->uid.uidByte[3]==EEPROM.read(EEPROM_CARD_OFFSET+3)) {
        number_of_cards=(uint16_t)(EEPROM.read(2))+(((uint16_t)(EEPROM.read(3)))<<8);
        EEPROM.write(4*number_of_cards+EEPROM_CARD_OFFSET, rfid->uid.uidByte[0]);
        EEPROM.write(4*number_of_cards+EEPROM_CARD_OFFSET+1, rfid->uid.uidByte[1]);
        EEPROM.write(4*number_of_cards+EEPROM_CARD_OFFSET+2, rfid->uid.uidByte[2]);
        EEPROM.write(4*number_of_cards+EEPROM_CARD_OFFSET+3, rfid->uid.uidByte[3]);
        EEPROM.write(2,number_of_cards+1 & 0xff);
        EEPROM.write(3,((number_of_cards+1)>>8)&0xff);
        EEPROM.commit();
        Serial.println("ADD_USER_CARD");
        response.state = ADD;
        return response;
  }

  if (rfid->uid.uidByte[0]==EEPROM.read(2*EEPROM_CARD_OFFSET) && rfid->uid.uidByte[1]==EEPROM.read(2*EEPROM_CARD_OFFSET+1)
      && rfid->uid.uidByte[2]==EEPROM.read(2*EEPROM_CARD_OFFSET+2) && rfid->uid.uidByte[3]==EEPROM.read(2*EEPROM_CARD_OFFSET+3)) {
        EEPROM.write(0, 0);
        EEPROM.write(1, 0);
        EEPROM.write(2, 2);
        EEPROM.write(3, 0);
        for (size_t ii = 0; ii < number_of_cards; ii++) {
          EEPROM.write(4*ii, 0);
          EEPROM.write(4*ii+1, 0);
          EEPROM.write(4*ii+2, 0);
          EEPROM.write(4*ii+3, 0);
        }

        EEPROM.commit();
        response.state = RESET;
        return response;
  }
  response.uuid[0] = rfid->uid.uidByte[0];
  response.uuid[1] = rfid->uid.uidByte[1];
  response.uuid[2] = rfid->uid.uidByte[2];
  response.uuid[3] = rfid->uid.uidByte[3];
  response.state = USER;
  return response;
}

void collect_weight() {
  String logStr;
  int count=0;
  int condition, _condition[END_CELL];
  logStr = "";
    // delay(50);
    condition = 0;
    for (size_t ii = START_CELL; ii < END_CELL; ii++)
      _condition[ii] = 0;
    do {
      ESP.wdtFeed();
      for (size_t ii = START_CELL; ii < END_CELL; ii++) {
        if(_condition[ii] == 0){
          switch_cell_channel(ii);
          update[ii] = LoadCell[ii].update();
          _condition[ii] = (LoadCell[ii].getReadIndex() == DATA_SET-1 && update[ii] == 1);
          condition += _condition[ii];
        }
        // Serial.println("_condition[" + String(ii) + "]:" + String(_condition[ii]));
        // Serial.println("condition:" + String(condition));
      }
    } while (condition < END_CELL-START_CELL);    //  && count<50
    // client.publish("log2",logStr.c_str());
    for (size_t ii = START_CELL; ii < END_CELL; ii++) {
      weights[ii] = LoadCell[ii].getData();
      logStr += " w[" + String(ii) + "]:" + String(weights[ii]);
    }
  // client.publish("log2",logStr.c_str());
  Serial.println(logStr);

  // logStr = "";
  // for (size_t ii = START_CELL; ii < END_CELL; ii++) {
  //   switch_cell_channel(ii);
  //   // delay(50);
  //   ESP.wdtFeed();
  //   do {
  //     update[ii] = LoadCell[ii].update();
  //     // delay(50);
  //     // count++;
  //     // if(update[ii] != 0)
  //       // logStr += "R[" + String(ii) + "]:" + String(LoadCell[ii].getReadIndex());
  //       // Serial.println("getReadIndex:" + String(LoadCell[ii].getReadIndex()) + "update:" + String(update[ii]));
  //   } while (!(LoadCell[ii].getReadIndex() == DATA_SET-1 && update[ii] == 1));    //  && count<50
  //   // client.publish("log2",logStr.c_str());
  //   weights[ii] = LoadCell[ii].getData();
  //   logStr += " w[" + String(ii) + "]:" + String(weights[ii]);
  // }
  // client.publish("log2",logStr.c_str());
  // Serial.println(logStr);
}

void set_leds(int R, int G, int B){
  switch_cell_channel(13);
  delay(10);
  for (uint16_t i = 0; i < 4*strip.numPixels()/10; i++)
  {
    // if (i % 4 == 0 || i % 4 == 3)
    //   continue;
    strip.setPixelColor(i, strip.Color(255, 39, 0));
  }
  for (uint16_t i = 4*strip.numPixels()/10; i < strip.numPixels(); i++)
  {
    // if (i % 4 == 0 || i % 4 == 3)
    //   continue;
    strip.setPixelColor(i, strip.Color(R, G, B));
  }
  strip.show();
}

bool offset_update(){
  {
  StaticJsonBuffer<1000> jsonBuffer, jsonBuffer2;
  String output;
  int temp = 0;

  JsonObject& root = jsonBuffer.createObject();
  root["did"] = station_id;
  root.printTo(output);
  client.publish("setting",output.c_str());
  while(!setup_flag && temp<40){
    httpServer.handleClient();
    mqtt_reconnect();
    client.loop();
    delay(100);
    temp++;
  }
  Serial.println("setup_flag:" + String(setup_flag));
  client.publish("log",("setup_flag:" + String(setup_flag)).c_str());
  if(!setup_flag)
    return false;
  JsonArray& array1 = jsonBuffer.parseArray(update_string);
  for (size_t ii = START_CELL; ii < END_CELL; ii++){
    JsonObject& obj = jsonBuffer2.parseObject(array1[ii].as<String>());
    offset[ii] = obj["off"+String(ii)];
    scale[ii] = obj["scl"+String(ii)];
  }
  }

  StaticJsonBuffer<1000> jsonBuffer3;
  String output;
  if (offset_flag[selected_cell]){
    offset_flag[selected_cell] = false;

    digitalWrite(HX711_CLK, LOW);
    for (size_t ii = selected_cell; ii <= second_selected_cell; ii++){
      switch_cell_channel(ii);
      ESP.wdtFeed();
      LoadCell[ii].tare();                //  tareNoDelay();
      client.publish("log","Tare Start");
      while (LoadCell[ii].getTareStatus() != true){
        httpServer.handleClient();
        mqtt_reconnect();
        client.loop();
        delay(50);
      }
      Serial.println("Tare cell # of " + String(ii) + " complete");
      client.publish("log",("Tare cell # of " + String(ii) + " complete").c_str());
      delay(100);
      offset[ii] = LoadCell[ii].getTareOffset();
    }
    JsonObject& root1 = jsonBuffer3.createObject();
    root1["did"] = station_id;
    JsonArray& nestedArray = root1.createNestedArray("cache_data");
    for (size_t ii = 0; ii < 10; ii++){
      JsonObject& cache_data = nestedArray.createNestedObject();
      cache_data["off"+String(ii)] = offset[ii];
      cache_data["scl"+String(ii)] = scale[ii];
      Serial.println("offset["+String(ii)+"]"+String(offset[ii])+"scale["+String(ii)+"]"+String(scale[ii]));
    }
    root1.printTo(output);
    client.publish("setup",output.c_str());
  }

  setup_flag = false;
  return true;
}

bool scale_update(){
  {
  StaticJsonBuffer<1000> jsonBuffer, jsonBuffer2;
  String output, logStr;
  int temp = 0;

  JsonObject& root = jsonBuffer.createObject();
  root["did"] = station_id;
  root.printTo(output);
  client.publish("setting",output.c_str());
  while(!setup_flag && temp<40){
    httpServer.handleClient();
    mqtt_reconnect();
    client.loop();
    delay(100);
    temp++;
  }
  Serial.println("setup_flag:" + String(setup_flag));
  if(!setup_flag)
    return false;
  JsonArray& array1 = jsonBuffer.parseArray(update_string);
  for (size_t ii = START_CELL; ii < END_CELL; ii++){
    JsonObject& obj = jsonBuffer2.parseObject(array1[ii].as<String>());
    offset[ii] = obj["off"+String(ii)];
    scale[ii] = obj["scl"+String(ii)];
  }
  }

  String output, logStr;
  StaticJsonBuffer<1000> jsonBuffer3;
  if (scale_flag[selected_cell]){
    scale_flag[selected_cell] = false;

    for (size_t ii = 0; ii < 4; ii++)
      collect_weight();

    float lastFac = LoadCell[selected_cell].getCalFactor();
    scale[selected_cell] = lastFac*weights[selected_cell]/test_weight[selected_cell];
    logStr = "w[" + String(selected_cell) + "]:" + String(weights[selected_cell]);
    logStr += ";test:" + String(test_weight[selected_cell]);
    logStr += ";lastFac:" + String(lastFac);
    logStr += ";newFac:" + String(scale[selected_cell]);
    Serial.println(logStr);
    client.publish("log",logStr.c_str());

    JsonObject& root1 = jsonBuffer3.createObject();
    root1["did"] = station_id;
    JsonArray& nestedArray = root1.createNestedArray("cache_data");
    for (size_t ii = 0; ii < 10; ii++){
      JsonObject& cache_data = nestedArray.createNestedObject();
      cache_data["off"+String(ii)] = offset[ii];
      cache_data["scl"+String(ii)] = scale[ii];
      Serial.println("offset["+String(ii)+"]"+String(offset[ii])+"scale["+String(ii)+"]"+String(scale[ii]));
    }
    root1.printTo(output);
    client.publish("setup",output.c_str());
  }

  setup_flag = false;
  return true;
}

void setup_loadcells(){
  String logStr, logStr2;
  bool is_update = offset_update();
  is_update &= scale_update();
  ESP.wdtDisable();
  for (size_t ii = START_CELL; ii < END_CELL; ii++) {
    httpServer.handleClient();
    ESP.wdtFeed();
    switch_cell_channel(ii);
    delay(10);
    LoadCell[ii].begin();
    delay(50);
    LoadCell[ii].start(3000);
    // delay(2000);
    if(is_update){
      LoadCell[ii].setTareOffset(offset[ii]);
      LoadCell[ii].setCalFactor(scale[ii]);
    } else  {
      // LoadCell[ii].setTareOffset(offset[ii]);
      LoadCell[ii].setCalFactor(224.0);
    }
    Serial.println("offset["+String(ii)+"]"+String(offset[ii])+"scale["+String(ii)+"]"+String(scale[ii]));
    logStr2 = "Startup + tare is complete for # of " + String(ii);
    Serial.println(logStr2);
    client.publish("log",logStr2.c_str());
  }
  ESP.wdtEnable(2000);
  // client.publish("init", logStr.c_str());
}

bool login(RFID_RESPONSE response, int id){
  bool success = false;
  int temp = 0;
  StaticJsonBuffer<200> jsonBuffer;
  HTTPClient http;
  String request = "http://stagingsite.ir:8000/api/v1/mqtt/permit/";
  request += "?hey=764557334E4446534D356B5763464865336777707846516A4E4F4372304B5A32";
  // mqtt_reconnect();
  // client.loop();

  // JsonObject& root = jsonBuffer.createObject();
  // root["did"] = id;
  request += "&station_id=" + String(id);
  String uidString = String(response.uuid[0])+"."+String(response.uuid[1])+"."+String(response.uuid[2])+"."+String(response.uuid[3]);
  // root["rfid"] = uidString;
  request += "&rfid=" + uidString;

  // String output;
  // root.printTo(output);

  temp = 0;
  login_flag = false;
  // Serial.println("publish->permission;" + String(output));
  // client.publish("permission",output.c_str());
  http.begin(request);
  client.publish("log",request.c_str());
  // http.addHeader("Host", "stagingsite.ir:8000");
  // http.addHeader("Content-Type", "application/json");
  // http.addHeader("Cache-Control", "no-cache");
  Serial.println("wait");
  // while(!success && temp<40){
  //   mqtt_reconnect();
  //   client.loop();
  //   Serial.print(".");
  //   delay(50);
  //   temp++;
  //   success = login_flag;
  // }
  int httpCode = http.GET();
  if (httpCode > 0)
  // {
    // Serial.printf("[HTTP] GET... code: %d\n", httpCode);
    if (httpCode == HTTP_CODE_OK) {
      success = true;
      String payload = http.getString();
      client.publish("log",payload.c_str());
      // Serial.println(payload);
    }
  // } else {
    // Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
  // }
  http.end();
  return success;
}

void send_data(RFID_RESPONSE response, float* _weights, int id){
  String logStr = "";
  logStr += "sizeof(_weights):" + String(sizeof((int*)_weights)) + ";";
  StaticJsonBuffer<250> jsonBuffer;   // up to 256

  mqtt_reconnect();
  client.loop();

  JsonObject& root = jsonBuffer.createObject();
  root["type"] = 1;
  JsonObject& nested_body = root.createNestedObject("body");
  nested_body["did"] = id;
  String uidString = String(response.uuid[0])+"."+String(response.uuid[1])+"."+String(response.uuid[2])+"."+String(response.uuid[3]);
  nested_body["cid"] = uidString;
  JsonArray& nested_body_pwl = nested_body.createNestedArray("pwl");
  for (size_t jj = 0; jj < 10; jj++) {
    nested_body_pwl.add<float>(_weights[jj]);
    logStr += String(jj) + ":" + String(_weights[jj]) + ";";
  }
  // client.publish("log",logStr.c_str());

  String output;
  root.printTo(output);

  client.publish("action",output.c_str());
  Serial.println("publish->action;" + String(output));
}

void setup_lock(){
  switch_cell_channel(11);
  myservo.attach(LOCK_PWM);  // attaches the servo on pin 9 to the servo object
  myservo.write(40);
  delay(1000);
  myservo.detach();
}

void open_lock(){
  switch_cell_channel(11);
  myservo.attach(LOCK_PWM);
  myservo.write(85);
  delay(500);
  myservo.detach();
}

void close_lock(){
  switch_cell_channel(11);
  myservo.attach(LOCK_PWM);
  myservo.write(20);
  delay(500);
  myservo.detach();
}

void setup() {
  station_id = 1;
  selected_cell = -1;
  second_selected_cell = -1;
  // ESP.wdtDisable();
  Serial.begin(115200);

  pinMode(ADR1, OUTPUT);
  pinMode(ADR2, OUTPUT);
  pinMode(ADR3, OUTPUT);
  pinMode(ADR4, OUTPUT);

  setup_wifi();

  // setup OTA
  MDNS.begin(host);
  httpUpdater.setup(&httpServer);
  httpServer.begin();
  MDNS.addService("http", "tcp", 80);
  // http://192.168.43.96/
  Serial.printf("HTTPUpdateServer ready! Open http://%s.local/update in your browser\n", host);

  client.setServer(mqtt_server, 8883);
  mqtt_reconnect();
  client.loop();

  client.publish("init","connected to wifi");
  client.setCallback(sub_callback);
  String ip = String(WiFi.localIP());
  client.publish("init",WiFi.localIP().toString().c_str());

  switch_cell_channel(10);
  delay(10);
  rfid = new MFRC522(SS_PIN, RST_PIN); // Instance of the class
  Serial.println("before SPI");
  client.publish("log", "before SPI");
  SPI.begin(); // Init SPI bus
  EEPROM.begin(4096);
  EEPROM.write(0, 0x34);
  EEPROM.write(1, 0xa5);
  EEPROM.write(2, 2);
  EEPROM.write(3, 0);
  Serial.println("after SPI");
  client.publish("log", "after SPI");
  rfid->PCD_Init(); // Init MFRC522
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
  digitalWrite(HX711_CLK, LOW);

  mqtt_reconnect();
  // client.loop();
  setup_loadcells();

  set_leds(251, 20, 50);
  delay(500);
  set_leds(250, 250, 250);
  delay(500);
  set_leds(151, 20, 50);
  delay(500);
  set_leds(250, 250, 250);
  delay(500);
  set_leds(51, 20, 50);
  led_color = 0;

  client.publish("log", "after led");
  setup_lock();
  open_lock();
  digitalWrite(HX711_CLK, LOW);
  client.publish("log", "after lock");
}

void loop() {
  String logStr;
  unsigned long tt;
  bool login_success;

  httpServer.handleClient();
  mqtt_reconnect();
  client.loop();

  tt = millis();
  RFID_RESPONSE response = read_rfid();
  digitalWrite(HX711_CLK, LOW);
  Serial.println("\"read_rfid()\" run time = " +  String(millis()-tt) );
  tt = millis();
  collect_weight();
  Serial.println("\"collect_weight()\" run time = " +  String(millis()-tt) );
  switch (response.state) {
    case ADD:
      Serial.println("ADD");
      break;
    case RESET:
      Serial.println("RESET");
      break;
    case USER:
      Serial.println("USER");
      tt = millis();
      send_data(response, weights, station_id);
      login_success = login(response, station_id);
      Serial.println("login_success:" + String(login_success));
      logStr = "login_success:" + String(login_success) + " time of login:" + String(millis()-tt);
      client.publish("log", logStr.c_str());
      if(login_success){
        open_lock();
        set_leds(0, 255, 0); // set_leds(51, 20, 50);
        tt = millis();
        while (millis()<tt+3000) {
            collect_weight();
        }
        switch_cell_channel(10);
        int door = digitalRead(DOOR_SWITCH);
        while (door == HIGH) {
            httpServer.handleClient();
            mqtt_reconnect();
            client.loop();
            collect_weight();
            switch_cell_channel(10);
            door = digitalRead(DOOR_SWITCH);
        }
        for (size_t ii = 0; ii < 2; ii++){
          set_leds(0, 255, 0);
          collect_weight();
          set_leds(255, 39, 0);
          collect_weight();
        }
        set_leds(0, 255, 0);
        close_lock();
        for (size_t ii = 0; ii < 2; ii++){
          set_leds(255, 39, 0);
          collect_weight();
          set_leds(0, 255, 0);
          collect_weight();
        }
        send_data(response, weights, station_id);
      } else {
        set_leds(255, 0, 0);
        delay(1000);
      }
      set_leds(0,191,255);
      // switch (led_color) {
      //   case 0:
      //     set_leds(0, 191, 255);
      //     break;
      //   case 1:
      //     set_leds(255,255,0);
      //     break;
      //   case 2:
      //     set_leds(255,255,20);
      //     break;
      //   case 3:
      //     set_leds(255,255,40);
      //     break;
      //   case 4:
      //     set_leds(255,255,60);
      //     break;
      //   case 5:
      //     set_leds(255,255,80);
      //     break;
      // }
      // led_color++;
      // if (led_color > 5)
      //   led_color = 0;

      // set_leds(250, 253, 253);
      break;
    case NO_VALID:
      Serial.println("NO_VALID");
      break;
    case CANT_READ:
      Serial.println("CANT_READ");
      break;
  }

  //****** Call Selected_cell ******
  if (selected_cell != -1){
    switch_cell_channel(selected_cell);
    Serial.println("selected_cell: " + String(selected_cell));

    if (offset_flag[selected_cell]) {
      if (offset_update())
        LoadCell[selected_cell].setTareOffset(offset[selected_cell]);
    } else if (scale_flag[selected_cell]){
      if (scale_update())
        LoadCell[selected_cell].setCalFactor(scale[selected_cell]);
    }
    if (weight_publish_flag[selected_cell] == true){
      // send_data(response, weights, station_id);
      for (size_t ii = 0; ii < 6; ii++)
        collect_weight();
      for (size_t ii = START_CELL; ii < END_CELL; ii++) {
        weights[ii] = LoadCell[ii].getData();
        logStr += " w[" + String(ii) + "]:" + String(weights[ii]);
      }
      client.publish("log2",logStr.c_str());
      weight_publish_flag[selected_cell] = false;
    }
    selected_cell = -1;
    second_selected_cell = -1;
  }
  if (status_door_flag){
    switch_cell_channel(10);
    logStr = "STATUS::DOOR_SWITCH:" + String(digitalRead(DOOR_SWITCH));
    client.publish("log", logStr.c_str());
    status_door_flag = false;
  }
  if (status_ip_flag){
    client.publish("log", WiFi.localIP().toString().c_str());
    status_ip_flag = false;
  }
}
